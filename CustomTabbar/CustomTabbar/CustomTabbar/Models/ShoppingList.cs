using System.Windows.Input;

namespace CustomTabbar.Models
{
    public class ShoppingList
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public bool IsOpened { get; set; }
        
        public int ItemsCount { get; set; }
        
        public ICommand DoneClickCommand { get; set; }
    }
}