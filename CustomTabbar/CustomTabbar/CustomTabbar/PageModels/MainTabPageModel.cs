using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CustomTabbar.Interfaces;
using CustomTabbar.Models;
using FreshMvvm;
using PropertyChanged;

namespace CustomTabbar.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class MainTabPageModel : FreshBasePageModel, IMainTabPageModel
    {
        public event Action<ShoppingList> OnItemClosed;
        public event Action<ShoppingList> OnItemAdded;
        public ClosedListsPageModel ClosedListsPageModel { get; set; }
        public OpenedListsPageModel OpenedListsPageModel { get; set; }
        public ICommand CenterButtonClickedCommand => new FreshAwaitCommand(OnCenterButtonClicked);

        public MainTabPageModel()
        {
            ClosedListsPageModel = new ClosedListsPageModel(this);
            OpenedListsPageModel = new OpenedListsPageModel(this);
        }

        private async void OnCenterButtonClicked(object parameter, TaskCompletionSource<bool> taskCompletionSource)
        {
            await CoreMethods.PushPageModel<AddNewListPageModel>(null, true);
        }

        public override void ReverseInit(object returnedData)
        {
            if (returnedData is ShoppingList shoppingList)
            {
                ItemAdded(shoppingList);
            }
        }

        public void ItemClosed(ShoppingList shoppingList)
        {
            OnItemClosed?.Invoke(shoppingList);
        }

        private void ItemAdded(ShoppingList shoppingList)
        {
            OnItemAdded?.Invoke(shoppingList);
        }
    }
}