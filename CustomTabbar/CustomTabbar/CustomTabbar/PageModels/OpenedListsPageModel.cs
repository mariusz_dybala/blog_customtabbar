using System.Collections.ObjectModel;
using System.Linq;
using CustomTabbar.Interfaces;
using CustomTabbar.Models;
using FreshMvvm;
using PropertyChanged;
using Xamarin.Forms;

namespace CustomTabbar.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class OpenedListsPageModel : FreshBasePageModel
    {
        private readonly IMainTabPageModel _mainTabPageModel;
        public ObservableCollection<ShoppingList> OpenedLists { get; set; }

        public OpenedListsPageModel(IMainTabPageModel mainTabPageModel)
        {
            _mainTabPageModel = mainTabPageModel;

            _mainTabPageModel.OnItemAdded += OnOnItemAdded;
            
            OpenedLists = new ObservableCollection<ShoppingList>
            {
                new ShoppingList
                {
                    Id = "0",
                    Name = "Saturday shopping",
                    Description = "We usually buy the food for the entire week",
                    IsOpened = true,
                    Owner = "Mariusz",
                    Type = "Meals",
                    ItemsCount = 10,
                    DoneClickCommand = new Command<string>(OnDoneList)
                },
                new ShoppingList
                {
                    Id = "1",
                    Name = "Party time",
                    Description = "It's weekend! Party time :)",
                    IsOpened = true,
                    Owner = "Mariusz",
                    Type = "Drinks, Snacks",
                    ItemsCount = 8,
                    DoneClickCommand = new Command<string>(OnDoneList)
                }
            };
        }

        private void OnOnItemAdded(ShoppingList shoppingList)
        {
            shoppingList.DoneClickCommand = new Command<string>(OnDoneList);
            OpenedLists.Add(shoppingList);
        }

        private void OnDoneList(string listId)
        {
            var listToClose = OpenedLists.First(x => string.Equals(x.Id, listId));

            listToClose.IsOpened = false;

            OpenedLists.Remove(listToClose);
            _mainTabPageModel.ItemClosed(listToClose);
        }
    }
}