using System;
using System.Windows.Input;
using CustomTabbar.Models;
using FreshMvvm;
using PropertyChanged;
using Xamarin.Forms;

namespace CustomTabbar.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class AddNewListPageModel : FreshBasePageModel
    {
        private string _listName;

        public string ListName
        {
            get => _listName;
            set => _listName = value;
        }

        private string _description;

        public string Description
        {
            get => _description;
            set => _description = value;
        }

        private string _owner;

        public string Owner
        {
            get => _owner;
            set => _owner = value;
        }

        private string _type;

        public string Type
        {
            get => _type;
            set => _type = value;
        }

        public ICommand AddClickedCommand => new Command(OnAddClicked);
        public ICommand CancelClickedCommand => new Command(OnCancelClicked);

        private void OnAddClicked()
        {
            var itemsCount = new Random().Next(1, 50);
            var itemId = Guid.NewGuid();

            var newShoppingList = new ShoppingList
            {
                Description = Description,
                IsOpened = true,
                ItemsCount = itemsCount,
                Id = itemId.ToString(),
                Owner = Owner,
                Name = ListName,
                Type = Type
            };

            CoreMethods.PopPageModel(newShoppingList, true, true);
        }

        private void OnCancelClicked()
        {
            CoreMethods.PopPageModel(true, true);
        }
    }
}