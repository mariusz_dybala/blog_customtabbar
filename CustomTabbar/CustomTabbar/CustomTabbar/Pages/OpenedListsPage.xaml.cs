using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomTabbar.Controls;
using CustomTabbar.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CustomTabbar.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenedListsPage : TabPage
    {
        public override StackLayout SelectionIndicator => (StackLayout) GetTemplateChild("OpenedListsTabSelector");
        public OpenedListsPage()
        {
            InitializeComponent();
            
            TabIndex = ((TabButton) GetTemplateChild("OpenedListsTab")).TabButtonIndex;
        }
    }
}