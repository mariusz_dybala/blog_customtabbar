using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using CustomTabbar.Interfaces;
using FreshMvvm;
using Xamarin.Forms;

namespace CustomTabbar.Pages
{
    public abstract class BaseTabNavigationPage : MultiPage<FreshNavigationContainer>
    {
        protected abstract IList<FreshNavigationContainer> Tabs { get; set; }
        protected abstract void CreatePages();
        protected abstract void SetBindingContextForTabs(object context);
        protected abstract void CreateStackNavigationForPages();

        public static BindableProperty ActiveTabIndexProperty =
            BindableProperty.Create(nameof(ActiveTabIndex), typeof(int), typeof(BaseTabNavigationPage),
                defaultBindingMode: BindingMode.TwoWay);

        public int ActiveTabIndex
        {
            get => (int) GetValue(ActiveTabIndexProperty);
            set => SetValue(ActiveTabIndexProperty, value);
        }

        public static BindableProperty CenterButtonClickedCommandProperty =
            BindableProperty.Create(nameof(CenterButtonClickedCommand), typeof(ICommand),
                typeof(BaseTabNavigationPage));

        public ICommand CenterButtonClickedCommand
        {
            get => (ICommand) GetValue(CenterButtonClickedCommandProperty);
            set => SetValue(CenterButtonClickedCommandProperty, value);
        }

        protected BaseTabNavigationPage()
        {
            BindingContextChanged += OnBindingContextChanged;
        }

        private void OnBindingContextChanged(object sender, EventArgs e)
        {
            CreatePages();
            SetBindingContextForTabs(BindingContext);
            CreateStackNavigationForPages();
            AddPages();
        }

        private void AddPages()
        {
            var pages = Tabs.OrderBy(x => x.RootPage.TabIndex);

            foreach (var tab in pages)
            {
                if (tab.RootPage is TabPage tabPage)
                {
                    tab.IsVisible = false;
                    foreach (var tabButton in tabPage.TabButtons)
                    {
                        tabButton.Clicked += (x) => TabOnTabGoVisible(tabButton.TabButtonIndex);
                    }

                    tabPage.CenterButton.Clicked += CenterButtonOnClicked;

                    Children.Add(tab);
                }
            }

            SetFirstTabVisibility(Children[0]);
        }

        private void SetFirstTabVisibility(FreshNavigationContainer firstTabPage)
        {
            if (firstTabPage.RootPage is TabPage tabPage)
            {
                firstTabPage.IsVisible = true;
                tabPage.ToggleTabSelection(true);
            }
        }

        private void CenterButtonOnClicked(object sender, EventArgs e)
        {
            CenterButtonClickedCommand?.Execute(null);
        }

        private void TabOnTabGoVisible(int index)
        {
            foreach (var tab in Children)
            {
                if (tab.RootPage is TabPage tabPage)
                {
                    var isSelected = tabPage.TabIndex == index;
                    tab.IsVisible = isSelected;
                    tabPage.ToggleTabSelection(isSelected);
                }
            }

            SetActiveTabIndexIfNeeded(index);
        }

        private void SetActiveTabIndexIfNeeded(int index)
        {
            if (index == ActiveTabIndex)
            {
                return;
            }

            ActiveTabIndex = index;
        }

        protected override FreshNavigationContainer CreateDefault(object item)
        {
            return default(FreshNavigationContainer);
        }
    }
}