using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomTabbar.Interfaces;
using CustomTabbar.PageModels;
using FreshMvvm;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CustomTabbar.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainTabPage : BaseTabNavigationPage
    {
        private TabPage _closedListsPage;
        private TabPage _openedListsPage;
        protected override IList<FreshNavigationContainer> Tabs { get; set; }

        public MainTabPage()
        {
            InitializeComponent();
        }

        protected override void CreatePages()
        {
            _closedListsPage = new ClosedListsPage();
            _openedListsPage = new OpenedListsPage();
        }

        protected override void CreateStackNavigationForPages()
        {
            var navigationClosedListsPage = new FreshNavigationContainer(_closedListsPage);
            var navigationOpenedListsPage = new FreshNavigationContainer(_openedListsPage);
            
            Tabs = new List<FreshNavigationContainer>
            {
                navigationOpenedListsPage,
                navigationClosedListsPage
            };
        }

        protected override void SetBindingContextForTabs(object context)
        {
            if (context is MainTabPageModel mainTabPageModel)
            {
                _closedListsPage.BindingContext = mainTabPageModel.ClosedListsPageModel;
                _openedListsPage.BindingContext = mainTabPageModel.OpenedListsPageModel;
            }
        }
    }
}