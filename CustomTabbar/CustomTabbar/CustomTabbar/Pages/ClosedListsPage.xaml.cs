using System;
using System.Collections.Generic;
using CustomTabbar.Controls;
using CustomTabbar.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CustomTabbar.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClosedListsPage : TabPage
    {
        public override StackLayout SelectionIndicator => (StackLayout) GetTemplateChild("ClosedListsTabSelector");

        public ClosedListsPage()
        {
            InitializeComponent();

            TabIndex = ((TabButton) GetTemplateChild("ClosedListsTab")).TabButtonIndex;
        }
    }
}