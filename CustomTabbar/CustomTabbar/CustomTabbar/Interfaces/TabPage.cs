using System;
using System.Collections.Generic;
using System.Windows.Input;
using CustomTabbar.Controls;
using Xamarin.Forms;

namespace CustomTabbar.Interfaces
{
    public abstract class TabPage : ContentPage
    {
        public  IList<TabButton> TabButtons { get; private set; }
        public  Button CenterButton { get; private set;}

        public abstract StackLayout SelectionIndicator { get; }
        
        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            
            TabButtons = new List<TabButton> {(TabButton)GetTemplateChild("OpenedListsTab"), (TabButton)GetTemplateChild("ClosedListsTab")};
            CenterButton = (Button) GetTemplateChild("CenterButtonControl");
        }

        public void ToggleTabSelection(bool isSelected)
        {
            SelectionIndicator.BackgroundColor = isSelected ? Color.Tomato : Color.LightGray;
        }
    }
}