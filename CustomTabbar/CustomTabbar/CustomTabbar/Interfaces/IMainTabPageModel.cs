using System;
using CustomTabbar.Models;
using CustomTabbar.PageModels;
using CustomTabbar.Pages;

namespace CustomTabbar.Interfaces
{
    public interface IMainTabPageModel
    {
        void ItemClosed(ShoppingList shoppingList);
        event Action<ShoppingList> OnItemClosed;
        event Action<ShoppingList> OnItemAdded;
    }
}