using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CustomTabbar.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabButton : StackButton
    {
        public int TabButtonIndex { get; set; }
        public FileImageSource TabImageSource
        {
            set => TabIcon.Source = value;
        }
        
        public TabButton()
        {
            InitializeComponent();
        }
    }
}